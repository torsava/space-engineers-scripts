#!/bin/bash

FILE=${1:-starship_computer_core.cs}
echo "Monitoring file ${FILE}..."

OTIME=`stat -c %Z ${FILE}`
while true; do
   ATIME=`stat -c %Z ${FILE}`

   if [[ "$ATIME" != "$OTIME" ]]; then    
       cat ${FILE} | sed -n '/^#endregion/,/^#region PreludeFooter/{//!p;}' | clip
       echo "$(date +'%r') - Copied ${FILE} to clipboard"
       OTIME=$ATIME
   fi
   sleep 1
done