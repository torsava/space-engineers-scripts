#region Prelude
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

// Change this namespace for each script you create.
namespace SpaceEngineers.GNeps.DefiantComputerCore {
    public sealed class Program : MyGridProgram {
    // Your code goes between the next #endregion and #region







#endregion
// Programmable Block script
//
// Starship Computer Core
// 
// Help:
// Gas Management (GM):
// - Configure gas tank fill levels in the CustomData of this Programmable block
// - Name a block to receive CustomData updates with info with [GM-OUT] in the name
// - Name or group your O2/H2 generators with [GM-GENS] in the name
// - Name or group your oxygen tanks with [GM-O] in the name
// - Name or group your hydrogen tanks with [GM-H] in the name
// - Name or group your hydrogen thrusters with [GM-H-THR] in the name
//
// Simple Airlocks:
// - Set up inner/outer doors and LCDs with names [SAXI] resp. [SAXO], for X between 0 and 9 (including).
// - Set up sensor actions to point to the programmable block with parameters:
//   - SA 0 inner in
//   - SA 0 inner out
//   - SA 0 outer in
//   - SA 0 outer out
//
// Advanced Airlocks:
//   - TODO
//
// Hangar:
// - Set up sensor actions to point to the programmable block with parameters:
//   - Hangar 1 outer in
//   - Hangar 1 outer out


// TODO:
// - Turn off unused airvents (they consume 100kW!!)
// - Finish hangar
//   - Sensor: Close hangar after ship passes through, open hangar when friendly ship is nearby
// - Close opened doors named [AutoClose]
// - Ship to Ship connector: Program/Timer block to disable sensor, open door and extend
//   - IMyShipConnector

#region Settings
// Script settings
static float PRESSURE_DIFFERENCE_SMALL = 0.01f;
//static float PRESSURE_DIFFERENCE_MID = 0.5f; // TODO needed?
        
#endregion

#region Globals
// Function objects
GasManager _gasManager;
Dictionary<int, AdvancedAirlock> _advancedAirlocks = new Dictionary<int, AdvancedAirlock>();
Dictionary<int, SimpleAirlock> _simpleAirlocks = new Dictionary<int, SimpleAirlock>();

Dictionary<int, Hangar> _hangars = new Dictionary<int, Hangar>();

// Misc
int _tickCounter = 0;

// Corutine state machine dict
Dictionary<string, IEnumerator<bool>> _stateMachines = new Dictionary<string, IEnumerator<bool>>();
List<string> _stateMachinesToRemove = new List<string>();

// Parsers
MyCommandLine _commandLine = new MyCommandLine();
MyIni _ini = new MyIni();

// LCD Colors
static Color DISABLED_AIRLOCK_COLOR = new Color(64, 0, 0); // red
static Color ENABLED_AIRLOCK_COLOR = new Color(0, 64, 0); // green
static Color MID_PRESSURE_DIFF_AIRLOCK_COLOR = new Color(64, 64, 0); // yellow
#endregion


#region Gas Manager
public class GasManager {
	// Gas manager scenarios:
	// 1. Normal - all tanks on, all gens off, tanks within parameters
	//    - 1(b) Low - all tanks on, all gens off, tanks low on gas, but no ice
	// 3. Refilling oxygen - oxy tanks on, hydro tanks off, gens on
	//    - Patients, bottles and ships will refill hydrogen using the gens
	// 4. Refilling hydrogen - oxy off, hydro on, gens on
	//    - Patients, bottles and airlocks that need to pressurize will refill oxygen using the gens
	//    - Airlocks that need to de-pressurize will *pause* the refilling to de-vent:
	//      - tanks on, gens off
	// 5. Emergency - gens on (but no ice), at least 1 gas system's tanks off because they're below Emergency level
	//    - Refilling - when ice in gens detected

	Program _p;
	
	IMyTerminalBlock _configBlock;
	IMyTerminalBlock _customDataOutput;
	List<IMyGasGenerator> _generators;
	TankArray _oxygenTanks;
	TankArray _hydrogenTanks;
	List<IMyThrust> _hydrogenThrusters;

	MyItemType _iceItem;
	bool _refillingInProgress = false;
	bool _refillingOxygen;
	int _deVentingAirlocks = 0;

    public GasManager(Program program, IMyTerminalBlock configBlock, IMyTerminalBlock customDataOutput, List<IMyGasGenerator> generators,
					  List<IMyGasTank> oxygenTanks, List<IMyGasTank> hydrogenTanks, List<IMyThrust> hydrogenThrusters) {
		_p = program;
		_configBlock = configBlock;
		_customDataOutput = customDataOutput;
		_generators = generators;
		_oxygenTanks = new TankArray(oxygenTanks);
		_hydrogenTanks = new TankArray(hydrogenTanks);
		_hydrogenThrusters = hydrogenThrusters;

		// Misc settings
		_iceItem = new MyItemType("MyObjectBuilder_Ore", "Ice");

		// Parse config data
		bool writeDefaults = _configBlock.CustomData == "";
		MyIniParseResult result;
		if (!_p._ini.TryParse(_configBlock.CustomData, out result)) {
			_p.Report("Error: Parsing Gas Management ini configuration\n\n" + result.ToString());
			throw new Exception(result.ToString());
		}
		_oxygenTanks.DesiredFillLevel = _p._ini.Get("Gas Management - Oxygen", "DesiredFillLevel").ToDouble(75);
		_oxygenTanks.RefillLevel = _p._ini.Get("Gas Management - Oxygen", "RefillLevel").ToDouble(50);
		_oxygenTanks.EmergencyReserveLevel = _p._ini.Get("Gas Management - Oxygen", "EmergencyReserveLevel").ToDouble(25);
		_hydrogenTanks.DesiredFillLevel = _p._ini.Get("Gas Management - Hydrogen", "DesiredFillLevel").ToDouble(95);
		_hydrogenTanks.RefillLevel = _p._ini.Get("Gas Management - Hydrogen", "RefillLevel").ToDouble(80);
		_hydrogenTanks.EmergencyReserveLevel = _p._ini.Get("Gas Management - Hydrogen", "EmergencyReserveLevel").ToDouble(25);

		if (writeDefaults) {
			_p._ini.Set("Gas Management - Oxygen", "DesiredFillLevel", _oxygenTanks.DesiredFillLevel);
			_p._ini.Set("Gas Management - Oxygen", "RefillLevel", _oxygenTanks.RefillLevel);
			_p._ini.Set("Gas Management - Oxygen", "EmergencyReserveLevel", _oxygenTanks.EmergencyReserveLevel);
			_p._ini.Set("Gas Management - Hydrogen", "DesiredFillLevel", _hydrogenTanks.DesiredFillLevel);
			_p._ini.Set("Gas Management - Hydrogen", "RefillLevel", _hydrogenTanks.RefillLevel);
			_p._ini.Set("Gas Management - Hydrogen", "EmergencyReserveLevel", _hydrogenTanks.EmergencyReserveLevel);
			_configBlock.CustomData = _p._ini.ToString();
		}

		// Stop any refilling in progress - will be restarted later if necessary
		StopRefilling();
	}
	
	public bool AnyDeVentingAirlock() {
		return _deVentingAirlocks > 0;
	}
	public void DeVentAirlock(bool start) {
		_deVentingAirlocks += start ? 1 : -1;

		if (_refillingInProgress && !_refillingOxygen) {
			// If we're refilling hydrogen, the oxygen tanks are disabled, so we need to pause the refilling to de-vent the airlock
			if (start) {
				StopRefilling();
			} else {
				StartRefilling();
			}
		}
	}

	public bool HydrogenThrustersEnabled() {
		if (_hydrogenThrusters == null) {
			return false;
		}
		return _hydrogenThrusters.Any(i => i.Enabled);
	}

	// Checks if at least one generator has ice
	public bool GeneratorsReady() {
		foreach(var generator in _generators) {
			if (generator.GetInventory().FindItem(_iceItem) != null) {
				return true;
			}
		}
		return false;
	}

	public bool CanRefill() {
		return GeneratorsReady() && !AnyDeVentingAirlock() && !HydrogenThrustersEnabled();
	}

	public void StartRefilling() {
		_oxygenTanks.Enabled = _refillingOxygen;
		_hydrogenTanks.Enabled = !_refillingOxygen;
		foreach(var generator in _generators) {
			generator.Enabled = true;
		}
	}				

	public void StopRefilling() {
		foreach(var generator in _generators) {
			generator.Enabled = false;
		}
		_oxygenTanks.Enabled = true;
		_hydrogenTanks.Enabled = true;
	}

	public string RefillingOxygenToString() {
		return _refillingOxygen ? "oxygen" : "hydrogen";
	}

	public void ReportStatus(string refillingStatus) {
		string status = refillingStatus + "\n" +
						"Oxygen: " + _oxygenTanks.Status() + "\n" +
						"Hydrogen: " + _hydrogenTanks.Status();

		_customDataOutput.CustomData = status;
	}

	public void ManageGas() {
		string refillingStatus = null;
		
		if (_refillingInProgress) {
			refillingStatus = "Refilling " + RefillingOxygenToString();

			var refilled = _refillingOxygen ? _oxygenTanks.Refilled() : _hydrogenTanks.Refilled();
			if (refilled) {
				_refillingInProgress = false;
				StopRefilling();
			}
			else {		
				double enabledFillLevel = _refillingOxygen ? _oxygenTanks.EnabledFillLevel() : _hydrogenTanks.EnabledFillLevel();
				if (!CanRefill() || enabledFillLevel == 100) {
					// Cannot refill any more (either generators don't have ice, airlocks need venting,
					// hydro thrusters are enabled, or all enabled tanks are full)
					_refillingInProgress = false;
					StopRefilling();
				}
			}
		}
		else {
			var oxyEmergency = _oxygenTanks.OnEmergencyReserveAndLock();
			var hydroEmergency = _hydrogenTanks.OnEmergencyReserveAndLock();
			var oxyRefill = _oxygenTanks.NeedsRefill();
			var hydroRefill = _hydrogenTanks.NeedsRefill();

			if (oxyRefill || hydroRefill) {
				// Refilling Oxygen is a priority, unless there's a hydroEmergency (but not oxyEmergency)
				_refillingOxygen = oxyEmergency || !hydroEmergency && oxyRefill;
				
				if(CanRefill()) {
					_refillingInProgress = true;
					
					StartRefilling();
				}
				else {
					if (!GeneratorsReady()) {
						refillingStatus = "Cannot refill: Generators don't have ice";
					}
					else if (AnyDeVentingAirlock()) {
						refillingStatus = "Refilling paused: De-venting airlock";
					}
					else if (HydrogenThrustersEnabled()) {
						refillingStatus = "Refilling paused: Hydrogen thrusters enabled";
					}
					else {
						refillingStatus = "Cannot refill: Unknown reason";

					}
				}
			}

			if (refillingStatus == null) {
				refillingStatus = "Gas levels nominal";
			}
		}

		// Write Gas Management status to a block to be displayed
		ReportStatus(refillingStatus);
	}

	#region TankArray
	public class TankArray {
		// Assumes all tanks in one array are of the same size
		List<IMyGasTank> _tanks;
		
		public TankArray(List<IMyGasTank> tanks) {
			_tanks = tanks;
		}
		
		public bool Enabled {
			get => _tanks.Any(x => x.Enabled);
			set {
				foreach (var tank in _tanks) {
					tank.Enabled = value;
				}
			}
		}

		double _desiredFillLevel;
		public double DesiredFillLevel {
			get { return _desiredFillLevel; }
			set { _desiredFillLevel = Math.Max(0, Math.Min(100, value)); }
		}

		double _refillLevel;
		public double RefillLevel {
			get { return _refillLevel; }
			set { _refillLevel = Math.Max(0, Math.Min(100, value)); }
		}

		double _emergencyReserveLevel;
		public double EmergencyReserveLevel {
			get { return _emergencyReserveLevel; }
			set { _emergencyReserveLevel = Math.Max(0, Math.Min(100, value)); }
		}

		public double FillLevel() {
			return _tanks.Sum(i => i.FilledRatio) / _tanks.Count * 100;
		}

		public double EnabledFillLevel() {
			var enabledTanks = _tanks.Where(i => i.Enabled);
			return enabledTanks.Sum(i => i.FilledRatio) / enabledTanks.Count() * 100;
		}
		
		public bool NeedsRefill() {
			return FillLevel() < RefillLevel;
		}

		public bool Refilled() {
			return FillLevel() >= DesiredFillLevel;
		}

		public bool OnEmergencyReserve() {
			return FillLevel() < EmergencyReserveLevel;
		}

		public bool OnEmergencyReserveAndLock() {
			if (OnEmergencyReserve()) {
				// Disable tanks if they're below Emergency Reserve level
				Enabled = false;
				return true;
			}
			return false;
		}

		public string Status() {
			string fill = FillLevel().ToString("0.00");
			string emergency = OnEmergencyReserve() ? " - EMERGENCY" : "";
			return $"{fill}%{emergency}\n" +
					$"Max: {DesiredFillLevel}%, Refill at: {RefillLevel}%, Emergency: {EmergencyReserveLevel}%";
		}
	}
	#endregion
}
#endregion


#region Hangar
public class Hangar {
	Program _p;
	// Hangar name should be name of the block that holds its configuration
	string _hangarName;
	
	// List<IMyAirtightHangarDoor> _hangarDoors;
	// List<IMyDoor> _innerDoors;
	// List<IMyAirVent> _vents;
	// List<IMyLightingBlock> _lights;
	// List<IMyTextPanel> _innerLCDs;
	// List<IMyTextPanel> _outerLCDs;
	// List<IMySoundBlock> _soundBlocks;

	// TODO LCD to say whether we have air inside
	
    public Hangar(Program program, string airlockConfigBlockName) {
		_p = program;
		_hangarName = airlockConfigBlockName;
		
		// IMyTerminalBlock hangarConfigBlock = _p.GetBlock<IMyTerminalBlock>(airlockConfigBlockName);
		
		// Parse config data
		// MyIniParseResult result;
		// if (!_p._ini.TryParse(hangarConfigBlock.CustomData, out result)) {
			// _p.Report("Error: Parsing hangar airlock ini configuration\n\n" + result.ToString());
			// throw new Exception(result.ToString());
		// }
		// _hangarDoors = _p.GetGroupBlocks<IMyAirtightHangarDoor>(_p._ini.Get("hangar", "hangarDoorsGroup").ToString());
	}
}
#endregion


#region AirlockSubsystems
public abstract class AbstractAirlockSubsystem<TBlock> : IEnumerable {

	protected Program _p;
	protected Dictionary<byte, TBlock> _items = new Dictionary<byte, TBlock>();
	protected Dictionary<byte, IEnumerable<KeyValuePair<byte, TBlock>>> _others = new Dictionary<byte, IEnumerable<KeyValuePair<byte, TBlock>>>();

	public AbstractAirlockSubsystem(Program program, byte zones, TBlock inner, TBlock outer, TBlock side) {
		_p = program;

		_items[0] = inner;
		_items[1] = outer;
		if (zones == 3) {
			_items[2] = side;
		}
	}

	public TBlock this[byte index] {
        get { return _items[index]; }
    }

	public IEnumerator GetEnumerator() => _items.GetEnumerator();

	public IEnumerable<KeyValuePair<byte, TBlock>> Others(byte zone) {
		if (!_others.ContainsKey(zone)) {
			_others[zone] = _items.Where(kvp => !zone.Equals(kvp.Key));
		}
		return _others[zone];
	}

	public IEnumerable<KeyValuePair<byte, TBlock>> Remaining(List<byte> zones) {
		if (zones.Count == 1) {
			return Others(zones[0]);
		}
		else if (zones.Count >= _items.Count) {
			// No zones are remaining
			return null;
		}
		else {
			return _items.Where(kvp => !zones.Contains(kvp.Key));
		}
	}

	// TODO remove these?
	// public bool Any(Func<TBlock,Boolean> func) {
	// 	// TODO remove Values?
	// 	return _items.Values.Any(func);
	// }
	// public bool All(Func<TBlock,Boolean> func) {
	// 	// TODO remove Values?
	// 	return _items.Values.All(func);
	// }

	public IEnumerable<KeyValuePair<byte, TBlock>> Where(Func<KeyValuePair<byte, TBlock>, bool> func) {
		return _items.Where(func);
	}
}

public class AirlockSubsystem<TBlock> : AbstractAirlockSubsystem<TBlock>
		where TBlock : IMyFunctionalBlock {
	public AirlockSubsystem(Program program, byte zones, TBlock inner, TBlock outer, TBlock side)
			: base(program, zones, inner, outer, side) {
	}

	public void Enable(IEnumerable<byte> zones, bool value) {
		if (zones != null) {
			foreach (var zone in zones) {
				_items[zone].Enabled = value;
			}
		}
	}
	public void Enable(IEnumerable<KeyValuePair<byte, TBlock>> zones, bool value) {
		if (zones != null) {
			foreach (var zoneKv in zones) {
				_items[zoneKv.Key].Enabled = value;
			}
		}
	}

	public void EnableAll(bool value) {
		Enable(_items, value);
	}
}

public class LCDsSubsystem : AbstractAirlockSubsystem<List<IMyTextPanel>> {
	public LCDsSubsystem(Program program, byte zones, List<IMyTextPanel> inner, List<IMyTextPanel> outer, List<IMyTextPanel> side)
			: base(program, zones, inner, outer, side) {
	}

	public void Setup(string airlockName, float fontSize, float textPadding) {
		// foreach (var lcd in _items) {
		// 	string name = airlockName;
		// 	// TODO read lcd.CustomData and parse custom LCD settings (lcd_text, lcd_fontsize)
		// 	if (lcd.CustomData != "") {

		// 	}
			
		// 	lcd.FontSize = fontSize;
		// 	lcd.TextPadding = textPadding;
		// 	lcd.WriteText(name, false);
		// }
	}
	
	public void BackgroundColor(byte zone, Color color) {
		foreach (var lcd in _items[zone]) {
			// TODO
			lcd.BackgroundColor = color;
		}
	}
}

public class VentSubsystem : AirlockSubsystem<IMyAirVent> {
	public VentSubsystem(Program program, byte zones, IMyAirVent inner, IMyAirVent outer, IMyAirVent side)
			: base(program, zones, inner, outer, side) {
	}

	public List<byte> EqualPressureZones(float oxygenLevel) {
		List<byte> zones = new List<byte>();
		foreach (var ventKvp in _items) {
			if (VentEqualPressure(oxygenLevel, ventKvp.Value)) {
				zones.Add(ventKvp.Key);
			}
		}
		return zones;
	}
}

public static bool VentEqualPressure(float pressure1, float pressure2) {
	return Math.Abs(pressure1 - pressure2) < PRESSURE_DIFFERENCE_SMALL;
}

public static bool VentEqualPressure(float pressure, IMyAirVent vent) {
	return vent == null && VentEqualPressure(pressure, 0) ||
		   vent != null && VentEqualPressure(pressure, vent.GetOxygenLevel());
}

public static bool IsVentPressureHigher(float pressure, IMyAirVent vent) {
	return vent == null && !VentEqualPressure(pressure, 0) ||
		   vent != null && pressure > vent.GetOxygenLevel();
}
#endregion


#region AdvancedAirlock
public class AdvancedAirlock {
	Program _p;
	// Timer _timer;
	
	int _id;
	IMyAirVent _airlockVent;
	AirlockSubsystem<IMyDoor> _doors;
	VentSubsystem _vents;
	AirlockSubsystem<IMySensorBlock> _sensors;
	LCDsSubsystem _LCDs;

	bool _changingPressure = false;
	bool _depressurizing = false;

	bool _manualOverride = false;
	
    public AdvancedAirlock(Program program, int airlockId, IMyAirVent airlockVent,
							IMyDoor innerDoor, IMyDoor outerDoor, IMyDoor sideDoor,
							IMyAirVent innerVent, IMyAirVent outerVent, IMyAirVent sideVent,
							IMySensorBlock innerSensor, IMySensorBlock outerSensor, IMySensorBlock sideSensor,
							List<IMyTextPanel> innerLCDs, List<IMyTextPanel> outerLCDs, List<IMyTextPanel> sideLCDs) {
		_p = program;
		_id = airlockId;
		_airlockVent = airlockVent;

		byte zones = sideDoor != null ? (byte) 3 : (byte) 2;
		_doors = new AirlockSubsystem<IMyDoor>(_p, zones, innerDoor, outerDoor, sideDoor);
		_vents = new VentSubsystem(_p, zones, innerVent, outerVent, sideVent);
		_sensors = new AirlockSubsystem<IMySensorBlock>(_p, zones, innerSensor, outerSensor, sideSensor);
		_LCDs = new LCDsSubsystem(_p, zones, innerLCDs, outerLCDs, sideLCDs);
		
		// TODO test if airlock is airtight
		// CanPressurize { get; }

		// TODO read LCD settings from the vent block
			// lines = b.CustomData.Split('\n'); 
			// foreach (string l in lines) 
			// { 
			//     part = l.Split(delim, 2, StringSplitOptions.RemoveEmptyEntries); 
			//     if (part.Length != 2) 
			//         continue; 
			//     if (part[0].Trim().ToUpper() == "FONT_SIZE") 
			//     { 
			//         if (float.TryParse(part[1].Trim(), out tempfloat)) 
			//         { 
			//             font_size = tempfloat; 
			//         } 
			//     } 
			//     if (part[0].Trim().ToUpper() == "NAME") 
			//     { 
			//             Name = part[1].Trim(); 
			//     } 
			//     if (part[0].Trim().ToUpper() == "DOORCLOSEDELAY") 
			//     { 
			//         if(float.TryParse(part[1].Trim(), out tempfloat)) 
			//             doorCloseDelay = tempfloat; 
			//     } 
					

			// } 

	}
	
	public string Id => $"sa_{_id}";

	public bool ManualOverrideToggle() {
		if (!_manualOverride) {
			_doors.EnableAll(true);
		}
		_manualOverride = !_manualOverride;
		return _manualOverride;
	}

	// TODOs:
	// - update LCDs
	//   - multiple LCDs per zone? And custom definition in CustomData
	// - if oxygen tanks are full or can't be enabled - just vent the airlock oxygen content to space
	//   - if oxygen tanks are empty and there's no ice - just open airlock anyway
	//   - display this information somewhere
	// - manual override for Radej - but close each door after X seconds
	// - finish misc todos
	// - implement knock knock while changing pressure!
	public void ManageAirlock() {
		var airlockPressure = _airlockVent.GetOxygenLevel();

		if (_changingPressure) {
			if (_depressurizing && VentEqualPressure(airlockPressure, 0)) {
				_changingPressure = false;
				_depressurizing = false;
				_p._gasManager.DeVentAirlock(false);
			}
			else if (!_depressurizing && VentEqualPressure(airlockPressure, 1)) {
				_changingPressure = false;
			}
		}
		
		var equalPressureZones = _vents.EqualPressureZones(airlockPressure);
		if (!_manualOverride) {
			// Enable/disable doors based on equality of pressure between the airlock and their zones
			_doors.Enable(equalPressureZones, true);
			_doors.Enable(_doors.Remaining(equalPressureZones), false);
		}

		// Handle sensors
		foreach (KeyValuePair<byte, IMyDoor> kvp in _doors) {
			var sensor = _sensors[kvp.Key];
			if (sensor == null) {
				// TODO
				// Check if door open, if so, set timer to close it
			}
			else {
				var door = kvp.Value;
				if (sensor.IsActive) {
					// Check if the door is already open/ing
					if (door.Status == DoorStatus.Closed || door.Status == DoorStatus.Closing) {
						// Check if the pressures are equal
						if (VentEqualPressure(airlockPressure, _vents[kvp.Key])) {
							door.OpenDoor();
						}
						else {
							bool higherPressure = IsVentPressureHigher(airlockPressure, _vents[kvp.Key]);

							// Check if we're already changing the pressure in the right direction
							if (!_changingPressure || _changingPressure && _depressurizing != higherPressure) {
								// Check if all doors are closed
								if (!_doors.Others(kvp.Key).Any(i => i.Value.Status != DoorStatus.Closed)) {
									_airlockVent.Depressurize = higherPressure;
									_changingPressure = true;
									
									if (higherPressure) {
										if (!_depressurizing) {
											_depressurizing = true;
											_p._gasManager.DeVentAirlock(true);
										}
									}
									else {
										if (_depressurizing) {
											_depressurizing = false;
											_p._gasManager.DeVentAirlock(false);
										}
									}
								}
							}
						}
					}
				}
				else {
					// Close door if it's open/ing
					if (door.Status == DoorStatus.Open || door.Status == DoorStatus.Opening) {
						door.CloseDoor();
					}
				}
			}
		}
	}
}
#endregion


#region SimpleAirlock
public class SimpleAirlock {
	Program _p;
	// Timer
	Timer _timer;
	
	int _simpleAirlockId;
	IMyDoor _innerDoor;
	IMyDoor _outerDoor;
	IMySensorBlock _innerSensor;
	IMySensorBlock _outerSensor;
	IMyTextPanel _innerLCD;
	IMyTextPanel _outerLCD;
	bool _innerKnockKnock = false;
	bool _outerKnockKnock = false;
	
    public SimpleAirlock(Program program, int simpleAirlockId,
						 IMyDoor innerDoor, IMyDoor outerDoor,
						 IMySensorBlock innerSensor, IMySensorBlock outerSensor,
						 IMyTextPanel innerLCD, IMyTextPanel outerLCD) {
		_p = program;
		_simpleAirlockId = simpleAirlockId;

		_innerDoor = innerDoor;
		_outerDoor = outerDoor;
		_innerLCD = innerLCD;
		_outerLCD = outerLCD;
		// TODO remove detecting sensors, it seems we can't set their actions anyway
		_innerSensor = innerSensor;
		_outerSensor = outerSensor;
		
		// Detect open doors and deactivate the others based on that
		if (_innerDoor.Status != DoorStatus.Closed) {
			if (_outerDoor.Status != DoorStatus.Closed) {
				// Both doors open, close both now
				_innerDoor.CloseDoor();
				_outerDoor.CloseDoor();
			}
			else {
				changeOuterDoor(false);
				if (_innerDoor.Status == DoorStatus.Closing) {
					WaitForDoorsToClose(true);
				}
			}
		}
		else if (_outerDoor.Status != DoorStatus.Closed) {
			changeInnerDoor(false);
			if (_outerDoor.Status == DoorStatus.Closing) {
				WaitForDoorsToClose(false);
			}
		}
		else {
			// Both doors closed, enable both to be sure
			changeInnerDoor(true);
			changeOuterDoor(true);
		}
	}
	
	public string Id => $"sa_{_simpleAirlockId}";
	
	private void changeInnerDoor(bool enabled) {
		_innerDoor.Enabled = enabled;
		if (_innerLCD != null) {
			// TODO
			// _innerLCD.BackgroundColor = enabled ? _p.ENABLED_AIRLOCK_COLOR : _p.DISABLED_AIRLOCK_COLOR;
		}
	}
	
	private void changeOuterDoor(bool enabled) {
		_outerDoor.Enabled = enabled;
		if (_outerLCD != null) {
			// TODO
			// _outerLCD.BackgroundColor = enabled ? _p.ENABLED_AIRLOCK_COLOR : _p.DISABLED_AIRLOCK_COLOR;
		}
	}
	
	public void WaitForDoorsToClose(bool inner) {
		_p.AddStateMachine(Id, WaitForDoorsToCloseStateMachine(inner));
	}
	
	public void CancelWaitForDoorsToCloseIfAny() {
		_p.CancelStateMachineIfExists(Id);
	}
	
	private IEnumerator<bool> WaitForDoorsToCloseStateMachine(bool inner) {
		string which = inner ? "inner" : "outer";
		for (_timer = new Timer(10, _p, $"Waiting for {which} door to close"); _timer.ActiveStatusDelayed(); ) {
			yield return _timer.YieldStatus;
			if (inner && _innerDoor.Status == DoorStatus.Closed || !inner && _outerDoor.Status == DoorStatus.Closed) {
				break;
			}
		}
		
		// Door is closed but not yet hermetically sealed, let's wait a bit
		for (_timer = new Timer(0.1, _p, "Waiting for hermetic seal"); _timer.ActiveStatus(); ) {
			yield return true;
		}
		
		// The door is closed, re-enable the opposite door
		// and let waiting people in
		if (inner) {
			changeOuterDoor(true);
			if (_outerKnockKnock) {
				handleSensor(false, true);
			}
		}
		else {
			changeInnerDoor(true);
			if (_innerKnockKnock) {
				handleSensor(true, true);
			}
		}
		_p.Report($"Finished: Waiting for {which} door to close");
		yield return true;
	}
	
	public void handleSensor(bool inner, bool inside) {
		// _p.Report($"Sensor detected {inner}, {inside}");
		if (inner) {
			if (inside) {
				// inner in
				if (_outerDoor.Status == DoorStatus.Closed) {
					CancelWaitForDoorsToCloseIfAny();
					changeOuterDoor(false);
					changeInnerDoor(true);
					_innerDoor.OpenDoor();
				}
				else {
					_innerKnockKnock = true;
				}
			}
			else {
				// inner out
				if (_innerDoor.Status != DoorStatus.Closed) {
					_innerDoor.CloseDoor();
					WaitForDoorsToClose(true);
				}
				_innerKnockKnock = false;
			}
		}
		else {
			if (inside) {
				// outer in
				if (_innerDoor.Status == DoorStatus.Closed) {
					CancelWaitForDoorsToCloseIfAny();
					changeInnerDoor(false);
					changeOuterDoor(true);
					_outerDoor.OpenDoor();
				}
				else {
					_outerKnockKnock = true;
				}
			}
			else {
				// outer out
				if (_outerDoor.Status != DoorStatus.Closed) {
					_outerDoor.CloseDoor();
					WaitForDoorsToClose(false);
				}
				_outerKnockKnock = false;
			}
		}
		return;
	}
}
#endregion


#region ProgramMain
public Program() {
	ReportClear();

	// Load Gas Management blocks
	IMyTerminalBlock configBlock = Me as IMyTerminalBlock; //Get1BlockUnderName<IMyTerminalBlock>("[GM-CONF]", true);
	IMyTerminalBlock customDataOutput = Get1BlockUnderName<IMyTerminalBlock>("[GM-OUT]", true);
	List<IMyGasGenerator> generators = GetBlocksUnderName<IMyGasGenerator>("[GM-GENS]", true);
	List<IMyGasTank> oxygenTanks = GetBlocksUnderName<IMyGasTank>("[GM-O]", true);
	List<IMyGasTank> hydrogenTanks = GetBlocksUnderName<IMyGasTank>("[GM-H]", true);
	List<IMyThrust> hydrogenThrusters = GetBlocksUnderName<IMyThrust>("[GM-H-THR]");
	if (configBlock != null && customDataOutput != null &&
			generators != null && generators.Count > 0 &&
			oxygenTanks != null && oxygenTanks.Count > 0 &&
			hydrogenTanks != null && hydrogenTanks.Count > 0) {
		_gasManager = new GasManager(this, configBlock, customDataOutput, generators, oxygenTanks, hydrogenTanks, hydrogenThrusters);

		Report("Gas Management running:");
		Report($"- Generators: {generators.Count}");
		Report($"- Oxygen tanks: {oxygenTanks.Count}");
		Report($"- Hydrogen tanks: {hydrogenTanks.Count}");
		Report($"- Hydrogen thrusters: {hydrogenThrusters.Count}\n");
	}
	else {
		Report("Gas Management didn't find all necessary blocks.\n");
	}

	// Let's load AdvancedAirlocks
	IMyAirVent airlockVent;
	IMyDoor innerDoor;
	IMyDoor outerDoor;
	IMyDoor sideDoor;
	IMyAirVent innerVent;
	IMyAirVent outerVent;
	IMyAirVent sideVent;
	IMySensorBlock innerSensor;
	IMySensorBlock outerSensor;
	IMySensorBlock sideSensor;
	List<IMyTextPanel> innerLCDs;
	List<IMyTextPanel> outerLCDs;
	List<IMyTextPanel> sideLCDs;
	
	for (var i = 0; i < 10; i++) {
		airlockVent = Get1BlockUnderName<IMyAirVent>($"[AA{i}V]");
		innerDoor = Get1BlockUnderName<IMyDoor>($"[AA{i}I]");
		outerDoor = Get1BlockUnderName<IMyDoor>($"[AA{i}O]");

		if (airlockVent != null && innerDoor != null && outerDoor != null) {	
			innerVent = Get1BlockUnderName<IMyAirVent>($"[AA{i}I]");
			outerVent = Get1BlockUnderName<IMyAirVent>($"[AA{i}O]");
			
			innerSensor = Get1BlockUnderName<IMySensorBlock>($"[AA{i}I]");
			outerSensor = Get1BlockUnderName<IMySensorBlock>($"[AA{i}O]");

			innerLCDs = GetBlocksUnderName<IMyTextPanel>($"[AA{i}I]");
			outerLCDs = GetBlocksUnderName<IMyTextPanel>($"[AA{i}O]");

			sideDoor = Get1BlockUnderName<IMyDoor>($"[AA{i}S]");
			if (sideDoor != null) {
				sideVent = Get1BlockUnderName<IMyAirVent>($"[AA{i}S]");
				sideSensor = Get1BlockUnderName<IMySensorBlock>($"[AA{i}S]");
				sideLCDs = GetBlocksUnderName<IMyTextPanel>($"[AA{i}S]");
			}
			else {
				sideVent = null;
				sideSensor = null;
				sideLCDs = null;
			}
		
			_advancedAirlocks.Add(i, new AdvancedAirlock(this, i, airlockVent,
									innerDoor, outerDoor, sideDoor,
									innerVent, outerVent, sideVent,
									innerSensor, outerSensor, sideSensor,
									innerLCDs, outerLCDs, sideLCDs));
		}
	}
	Report($"Advanced Airlocks loaded: {_advancedAirlocks.Count}");

	// Let's load all SimpleAirlocks
	IMyTextPanel innerLCD;
	IMyTextPanel outerLCD;
	for (var i = 0; i < 10; i++) {
		innerDoor = Get1BlockUnderName<IMyDoor>($"[SA{i}I]");
		outerDoor = Get1BlockUnderName<IMyDoor>($"[SA{i}O]");
		if (innerDoor != null && outerDoor != null) {	
			innerSensor = Get1BlockUnderName<IMySensorBlock>($"[SA{i}I]");
			outerSensor = Get1BlockUnderName<IMySensorBlock>($"[SA{i}O]");
			
			innerLCD = Get1BlockUnderName<IMyTextPanel>($"[SA{i}I]");
			outerLCD = Get1BlockUnderName<IMyTextPanel>($"[SA{i}O]");

			// Create the simple airlock object
			_simpleAirlocks.Add(i, new SimpleAirlock(this, i, innerDoor, outerDoor, innerSensor, outerSensor, innerLCD, outerLCD));
		}
	}
	
	Report($"Simple Airlocks loaded: {_simpleAirlocks.Count}");

	// By default run this every 10 ticks
	Runtime.UpdateFrequency = UpdateFrequency.Update10;
}

public void Main(string argument, UpdateType updateType) {
	// If the update source is from a trigger or a terminal,
	// this is an interactive command.
	if ((updateType & (UpdateType.Trigger | UpdateType.Terminal)) != 0) {
		if (_commandLine.TryParse(argument)) {
			string command = _commandLine.Argument(0);

			if (command == null) {
				Report("No command specified");
			} 
			else {
				if (string.Equals(command, "SA", StringComparison.OrdinalIgnoreCase)) {
					int airlockId = Int32.Parse(_commandLine.Argument(1));
					string side = _commandLine.Argument(2);
					if (side == null) {
						Report("Cannot operate airlock, inner/outer not specified");
						return;
					}
					string direction = _commandLine.Argument(3);
					if (direction == null) {
						Report("Cannot operate airlock, no direction specified");
						return;
					}
					bool inner = side == "inner";
					bool inside = direction == "in";

					if (_simpleAirlocks.ContainsKey(airlockId)) {
						_simpleAirlocks[airlockId].handleSensor(inner, inside);
					}
				}
				if (string.Equals(command, "AA", StringComparison.OrdinalIgnoreCase)) {
					int airlockId = Int32.Parse(_commandLine.Argument(1));
					if (!_advancedAirlocks.ContainsKey(airlockId)) {
						Report($"Couldn't find Advanced Airlock ID {airlockId}");
					}
					else {
						string op = _commandLine.Argument(2);
						if (string.Equals(op, "toggle", StringComparison.OrdinalIgnoreCase)) {
							_advancedAirlocks[airlockId].ManualOverrideToggle();
						}
						else {
							Report("Unknown operation {op}");
							return;
						}
					}
				}
				else if (string.Equals(command, "hangar", StringComparison.OrdinalIgnoreCase)) {
					//hangarName = _commandLine.Argument(1);
					// TODO
					//bool switchIsSet = _commandLine.Switch("myflag");
				}
				else {
					Report($"Unknown command {command}");
				}
			}
		}
	}
	
	// If the update source has the Update10 flag, it means
	// there are state machines to be run.
	if ((updateType & UpdateType.Update10) != 0) {
		// Advanced airlocks
		foreach(var advancedAirlock in _advancedAirlocks.Values) {
			advancedAirlock.ManageAirlock();
		}

		// Run any present state machines
		RunStateMachines();

		// Run Gas Management once every 10 runs so that it runs every 100 ticks
		if (++_tickCounter >= 10) {
			_tickCounter = 0;

			if (_gasManager != null) {
				_gasManager.ManageGas();
			}
		}
	}
	
	// If the update source has the Update100 flag, it means
	// there are no state machines and we'll do only regular maintenance
	// TODO slow processing was disabled
	// if ((updateType & UpdateType.Update100) != 0) {
	// 	if (_gasManager != null) {
	// 		_gasManager.ManageGas();
	// 	}
	// 	foreach(var advancedAirlock in _advancedAirlocks.Values) {
	// 		advancedAirlock.ManageAirlock();
	// 	}
	// }
}
#endregion


#region StateMachineFunctions
public void RunStateMachines() {
	foreach (var item in _stateMachines) {
		var stateMachine = item.Value;
		
		// If there is an active state machine, run its next instruction set.
		if (stateMachine != null) {
			// The MoveNext method is the most important part of this system. When you call
			// MoveNext, your method is invoked until it hits a `yield return` statement.
			// Once that happens, your method is halted and flow control returns _here_.
			// At this point, MoveNext will return `true` since there's more code in your
			// method to execute. Once your method reaches its end and there are no more
			// yields, MoveNext will return false to signal that the method has completed.
			// The actual return value of your yields are unimportant to the actual state
			// machine.
			bool hasMoreSteps = stateMachine.MoveNext();

			// If there are no more instructions, of if the corutine indicates it wants to stop (be yield return false),
			// we stop and release the state machine.
			if (!hasMoreSteps || !stateMachine.Current) {
				_stateMachinesToRemove.Add(item.Key);
			}
		}
		else {
			Report("It's null jim");
			_stateMachinesToRemove.Add(item.Key);
		}
	}
	
	foreach (var key in _stateMachinesToRemove) {		
		if (_stateMachines.ContainsKey(key)) {
			if (_stateMachines[key] != null) {
				_stateMachines[key].Dispose();
			}
			_stateMachines.Remove(key);
		}
	}
	_stateMachinesToRemove.Clear();
	
	if(_stateMachines.Count == 0) {
		// Slow down this script when last state machine has finished
		// TODO slowing down disabled
		//Runtime.UpdateFrequency = UpdateFrequency.Update100;
	}
}

public void AddStateMachine(string key, IEnumerator<bool> stateMachine) {
	if(_stateMachines.ContainsKey(key)) {
		Report($"Error: State machine for key `{key}` already exists.");
		return;
	}
	if(_stateMachines.Count == 0) {
		// Speed up running this script to take care of the new state machine(s)
		// TODO speeding up and slowing down disabled
		// Runtime.UpdateFrequency = UpdateFrequency.Update10;
		// _tickCounter = 0;
		
		// Clear out the remove queue as none of those processes were running.
		_stateMachinesToRemove.Clear();
	}
	_stateMachines[key] = stateMachine;
}

public void CancelStateMachineIfExists(string key) {
	_stateMachinesToRemove.Add(key);
}
#endregion


#region Report
public void Report(string msg) {
	//Me.CustomData = Me.CustomData + "\n" + msg; - TODO report to a different block than Me, that has configuration now
	Echo(msg);
}

public void ReportClear() {
	// TODO clear the output on start
}
#endregion


#region GetBlocks
public T GetBlock<T>(string blockName) where T : class {
	T block = GridTerminalSystem.GetBlockWithName(blockName) as T;
	if (block == null) {
		Report($"Error: Could not find block named `{blockName}`.");
		return null;
	}
	return block;
}

public List<T> GetGroupBlocks<T>(string groupName) where T : class {
	IMyBlockGroup blockGroup = GridTerminalSystem.GetBlockGroupWithName(groupName);
	if (blockGroup == null) {
		Report($"Error: Could not find group `{groupName}`.");
		return null;
	}
	
	List<T> blocks = new List<T>();
	blockGroup.GetBlocksOfType(blocks);
	if (!blocks.Any()) {
		Report($"Error: Group `{groupName}` has no blocks.");
		return null;
	}
	return blocks;
}

public T Get1BlockUnderName<T>(string name, bool errorMsg = false) where T : class {
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
	GridTerminalSystem.SearchBlocksOfName(name, blocks, block => block is T);
	if (blocks != null && blocks.Count > 0) {
		return blocks[0] as T;
	}
	if (errorMsg) {
		Report($"Error: Didn't find a block containing the name `{name}`.");
	}
	return null;
}

public List<T> GetBlocksUnderName<T>(string name, bool errorMsg = false) where T : class {
	// Gets blocks from a group containing this name and/or
	// all blocks containing the name
	List<T> wantedBlocks = new List<T>();

	// Let's find groups that contain the name
	List<IMyBlockGroup> groups = new List<IMyBlockGroup>();
	List<T> groupBlocks = new List<T>();
	GridTerminalSystem.GetBlockGroups(groups, group => group.Name.Contains(name));
	if (groups != null && groups.Count > 0) {
		foreach (var group in groups) {
			group.GetBlocksOfType(groupBlocks);
			wantedBlocks.AddRange(groupBlocks);
		}
	}

	// Let's find blocks that contain the name
	List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
	GridTerminalSystem.SearchBlocksOfName(name, blocks, block => block is T);
	if (blocks != null && blocks.Count > 0) {
		wantedBlocks.AddRange(blocks.Cast<T>());
	}

	if (!wantedBlocks.Any()) {
		if (errorMsg) {
			Report($"Error: Didn't find any blocks or groups with blocks containing the name `{name}`.");
		}
		return null;
	}
	return wantedBlocks;
}
#endregion


#region Timer
public class Timer {
	DateTime _endTime;
	Program _p;
	string _taskMsg;
	int _echoDots = 0;
	int _echoDotsWait = 0;
	
	public bool YieldStatus { get; set; } = true;
	
    public Timer(double seconds, Program program = null, string taskMsg = null) {
		_endTime = DateTime.Now.AddSeconds(seconds);
		_p = program;
		_taskMsg = taskMsg;
		if (_taskMsg == null) {
			_taskMsg = "Waiting";
		}
	}
	
    public bool Active {
		get { return DateTime.Now < _endTime; }
	}
	
	public double Remaining {
		get { return (_endTime - DateTime.Now).TotalSeconds; }
	}
	
	public bool ActiveStatus() {
		if (Active) {
			_p.Report($"{_taskMsg}" + new String('.', _echoDots));
			if (_echoDotsWait++ > 4) {
				_echoDots = (_echoDots + 1) % 4;
				_echoDotsWait = 0;
			}
		}
		else {
			_p.Report($"Error: {_taskMsg}");
		}
		return Active;
	}
	
	public bool ActiveStatusDelayed() {
		if (YieldStatus) {
			if(ActiveStatus()) {
				return true;
			}
			else {
				YieldStatus = false;
				return true;
			}
		}
		else {
			return false;
		}
	}
}
#endregion
#region PreludeFooter
    }
}
#endregion