#region Prelude
using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using VRageMath;
using VRage.Game;
using VRage.Collections;
using Sandbox.ModAPI.Ingame;
using VRage.Game.Components;
using VRage.Game.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game.EntityComponents;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ObjectBuilders.Definitions;

// Change this namespace for each script you create.
namespace SpaceEngineers.GNeps.DefiantComputerCore {
    public sealed class Program : MyGridProgram {
    // Your code goes between the next #endregion and #region







#endregion
// Programmable Block script
//
// Defiant: Solar Array Controller
// 
//
// Help:
// - Commands: deploy, retract, abort
// - Status is updated in the CustomData of the Programmable Block and can be displayed using Automatic LCDs 2 for example

// API index: https://github.com/malware-dev/MDK-SE/wiki/Api-Index
// Tutoral used: https://github.com/malware-dev/MDK-SE/wiki/Quick-Introduction-to-Space-Engineers-Ingame-Scripts


// Settings
// -- Port
// string solarArrayName = "Port";
// string weldersGroupName = "Defiant: Solar Array Welders (port)";
// string grindersGroupName = "Defiant: Solar Array Grinders (port)";
// string projectorsGroupName = "Defiant: Solar Array Projectors (port)";
// string basePistonsGroupName = "Defiant: Solar Array Base Pistons (port)";
// string servicePistonsGroupName = "Defiant: Solar Array Service Pistons (port)";
// string mainPistonsGroupName = "Defiant: Solar Array Main Pistons (port)";
// string rotorsGroupName = "Defiant: Solar Array Rotors (port)";
// string lcdsGroupName = "Defiant: Solar Array LCDs (port)";
// string solarComputerName = "Defiant: Solar Array Computer (port)";
// -- Starboard
string solarArrayName = "Starboard";
string weldersGroupName = "Defiant: Solar Array Welders (starboard)";
string grindersGroupName = "Defiant: Solar Array Grinders (starboard)";
string projectorsGroupName = "Defiant: Solar Array Projectors (starboard)";
string basePistonsGroupName = "Defiant: Solar Array Base Pistons (starboard)";
string servicePistonsGroupName = "Defiant: Solar Array Service Pistons (starboard)";
string mainPistonsGroupName = "Defiant: Solar Array Main Pistons (starboard)";
string rotorsGroupName = "Defiant: Solar Array Rotors (starboard)";
string lcdsGroupName = "Defiant: Solar Array LCDs (starboard)";
string solarComputerName = "Defiant: Solar Array Computer (starboard)";
// End of Settings


// Variables
// TODO - these should not be initialized, the new lists are thrown away
List<IMyShipWelder> welders = new List<IMyShipWelder>();
List<IMyShipGrinder> grinders = new List<IMyShipGrinder>();
List<IMyProjector> projectors = new List<IMyProjector>();
List<IMyPistonBase> basePistons = new List<IMyPistonBase>();
List<IMyPistonBase> servicePistons = new List<IMyPistonBase>();
List<IMyPistonBase> mainPistons = new List<IMyPistonBase>();
List<IMyMotorStator> rotors = new List<IMyMotorStator>();
List<IMyTextSurface> lcds = new List<IMyTextSurface>();
IMyProgrammableBlock solarComputer;
	
// Corutine state machine
IEnumerator<bool> _stateMachine;
// Argument parser
MyCommandLine _commandLine = new MyCommandLine();
// Timer
Timer timer;



public IEnumerator<bool> Test()
{	
	foreach (var lcd in lcds) {
		int underlineLen = solarArrayName == "Starboard" ? 20 : 16;
		string underline = new String('=', underlineLen);
		lcd.WriteText($"{solarArrayName} Solar Computer\n{underline}\n\nCurrently disabled.", false);
	}
	Report("Testing done");
	yield return true;
}

public IEnumerator<bool> Deploy()
{
	Report("Doing pre-start cleanup");
	foreach (var grinder in grinders) {
		grinder.Enabled = false;
	}
	
	// Extending base & service pistons
	foreach (var piston in basePistons) {
		piston.Velocity = 1;
		piston.Extend();
	}
	timer = new Timer(0.5);
	while (timer.Active) {
		yield return true;
	}
	foreach (var piston in servicePistons) {
		piston.Velocity = 1;
		piston.MaxLimit = 10;
		piston.Extend();
	}
	for (timer = new Timer(45, this, "Extending base and service pistons"); timer.ActiveStatusDelayed(); ) {
		yield return timer.YieldStatus;
		if (servicePistons.Concat(basePistons).All(x => x.Status == PistonStatus.Extended)) {
			break;
		}
	}

	Report("Enabling projectors, welders");
	foreach (var projector in projectors) {
		projector.Enabled = true;
	}
	foreach (var welder in welders) {
		welder.Enabled = true;
	}
	// Extending main pistons
	foreach (var piston in mainPistons) {
		piston.Velocity = 0.13f;
		piston.Extend();
	}
	for (timer = new Timer(120, this, "Extending the solar array"); timer.ActiveStatusDelayed(); ) {
		yield return timer.YieldStatus;
		if (mainPistons.All(x => x.Status == PistonStatus.Extended)) {
			break;
		}
	}

	
	// Partially retracting service pistons
	foreach (var piston in servicePistons) {
		piston.MinLimit = 8.3f;
		piston.Retract();
	}
	for (timer = new Timer(45, this, "Partially retracting service pistons"); timer.ActiveStatusDelayed(); ) {
		yield return timer.YieldStatus;
		if (servicePistons.All(x => x.Status == PistonStatus.Retracted)) {
			break;
		}
	}

	
	timer = new Timer(6, this, "Welding last panels");
	while (timer.ActiveStatus()) {
		yield return true;
	}
	
	Report("Disabling welders, projectors and retracting service pistons");
	foreach (var welder in welders) {
		welder.Enabled = false;
	}
	foreach (var projector in projectors) {
		projector.Enabled = false;
	}

	// Retracting service pistons to the parked position
	foreach (var piston in servicePistons) {
		piston.MinLimit = 3;
		piston.Retract();
	}
	for (timer = new Timer(45, this, "Parking service pistons"); timer.ActiveStatusDelayed(); ) {
		yield return timer.YieldStatus;
		if (servicePistons.All(x => x.Status == PistonStatus.Retracted)) {
			break;
		}
	}
	
	Report("Enabling Isy's Solar Alignment script");
	solarComputer.Enabled = true;
	
	Report("Solar array deployed");
	yield return true;
}



public IEnumerator<bool> Retract()
{
	Report("Doing pre-start cleanup");
	foreach (var welder in welders) {
		welder.Enabled = false;
	}
	foreach (var grinder in grinders) {
		grinder.Enabled = false;
	}
	foreach (var projector in projectors) {
		projector.Enabled = false;
	}
	
	Report("Disabling Isy's Solar Alignment script");
	solarComputer.Enabled = false;
	
	// Disable LCDs showing Isy's script
	foreach (var lcd in lcds) {
		int underlineLen = solarArrayName == "Starboard" ? 20 : 16;
		string underline = new String('=', underlineLen);
		lcd.WriteText($"{solarArrayName} Solar Computer\n{underline}\n\nCurrently disabled.", false);
	}
	
	// Fold the solar array
	// We turn the rotors one at a time, not to get into a situation where we bang against the adjacent gatling turret :)
	foreach (var rotor in rotors) {
		if (rotor.Angle < 0 || rotor.Angle > 3.14f) {
			rotor.UpperLimitDeg = 0;
			rotor.TargetVelocityRPM = 1;
		}
		else if (rotor.Angle > 0) {
			rotor.LowerLimitDeg = 0;
			rotor.TargetVelocityRPM = -1;
		}
		
		for (timer = new Timer(60, this, "Folding the solar array"); timer.ActiveStatusDelayed(); ) {
			yield return timer.YieldStatus;
			// Wait until the angles are less than 1 degree
			if (Math.Abs(rotor.Angle) < 0.015) {
				break;
			}
		}
	}
	
	// Stop the rotors and restore limits
	foreach (var rotor in rotors) {
		rotor.TargetVelocityRPM = 0;
		
		if (rotor.Name.Contains("2")) {
			rotor.LowerLimitDeg = -130;
			rotor.UpperLimitDeg = +145;
		}
		else {
			rotor.LowerLimitDeg = float.MinValue;
			rotor.UpperLimitDeg = float.MaxValue;
		}
	}

	// Extending service pistons
	foreach (var piston in servicePistons) {
		piston.Velocity = 1;
		piston.MaxLimit = 10;
		piston.Extend();
	}
	for (timer = new Timer(45, this, "Extending service pistons"); timer.ActiveStatusDelayed(); ) {
		yield return timer.YieldStatus;
		if (servicePistons.All(x => x.Status == PistonStatus.Extended)) {
			break;
		}
	}

	Report("Enabling grinders");
	foreach (var grinder in grinders) {
		grinder.Enabled = true;
	}
	timer = new Timer(3, this, "Grinding first set of panels");
	while (timer.ActiveStatus()) {
		yield return true;
	}
	
	// Retracting main pistons
	foreach (var piston in mainPistons) {
		//piston.Velocity = 0.113f;
		piston.Velocity = 0.25f;
		piston.Retract();
	}
	for (timer = new Timer(120, this, "Retracting the solar array"); timer.ActiveStatusDelayed(); ) {
		yield return timer.YieldStatus;
		if (mainPistons.All(x => x.Status == PistonStatus.Retracted)) {
			break;
		}
	}
	
	Report("Disabling grinders");
	foreach (var grinder in grinders) {
		grinder.Enabled = false;
	}
	// Retracting service and base pistons
	foreach (var piston in servicePistons) {
		piston.Velocity = 1;
		piston.MinLimit = 0;
		piston.Retract();
	}
	timer = new Timer(0.5);
	while (timer.Active) {
		yield return true;
	}
	foreach (var piston in basePistons) {
		piston.Velocity = 1;
		piston.Retract();
	}
	for (timer = new Timer(45, this, "Retracting service and base pistons"); timer.ActiveStatusDelayed(); ) {
		yield return timer.YieldStatus;
		if (servicePistons.Concat(basePistons).All(x => x.Status == PistonStatus.Retracted)) {
			break;
		}
	}
	
	Report("Solar array retracted");
	yield return true;
}



public void Abort()
{
	foreach (var welder in welders) {
		welder.Enabled = false;
	}
	foreach (var grinder in grinders) {
		grinder.Enabled = false;
	}
	foreach (var projector in projectors) {
		projector.Enabled = false;
	}
	foreach (var piston in servicePistons) {
		piston.Velocity = 1;
		piston.MinLimit = 0;
		piston.Retract();
	}
	foreach (var piston in mainPistons) {
		piston.Velocity = 0.13f;
		piston.Extend();
	}
	foreach (var piston in basePistons) {
		piston.Velocity = 1;
		piston.Extend();
	}
	foreach (var rotor in rotors) {
		rotor.TargetVelocityRPM = 0;
	}
	solarComputer.Enabled = false;
	
	// Report("Stopped grinders, welders, projectors, rotors, solar computer");
	// Report("Retracting service Pistons, extending base and main pistons");
	Report("Solar array procedure aborted");
}



public Program() 
{
}

public void Main(string argument, UpdateType updateType)
{
	// Lazy load the block groups
	if (!welders.Any() || !grinders.Any() || !projectors.Any() || !basePistons.Any() || !servicePistons.Any() || !mainPistons.Any() || !rotors.Any() || solarComputer == null) {
		Report("Retrieving block groups");
		
		IMyBlockGroup weldersGroup = GridTerminalSystem.GetBlockGroupWithName(weldersGroupName);
		IMyBlockGroup grindersGroup = GridTerminalSystem.GetBlockGroupWithName(grindersGroupName);
		IMyBlockGroup projectorsGroup = GridTerminalSystem.GetBlockGroupWithName(projectorsGroupName);
		IMyBlockGroup basePistonsGroup = GridTerminalSystem.GetBlockGroupWithName(basePistonsGroupName);
		IMyBlockGroup servicePistonsGroup = GridTerminalSystem.GetBlockGroupWithName(servicePistonsGroupName);
		IMyBlockGroup mainPistonsGroup = GridTerminalSystem.GetBlockGroupWithName(mainPistonsGroupName);
		IMyBlockGroup rotorsGroup = GridTerminalSystem.GetBlockGroupWithName(rotorsGroupName);
		IMyBlockGroup lcdsGroup = GridTerminalSystem.GetBlockGroupWithName(lcdsGroupName);
		if (weldersGroup == null || grindersGroup == null || basePistonsGroup == null || servicePistonsGroup == null || mainPistonsGroup == null || rotorsGroup == null || lcdsGroup == null) {
			Report("At least one group was not found");
			return;
		}
		weldersGroup.GetBlocksOfType(welders);
		grindersGroup.GetBlocksOfType(grinders);
		projectorsGroup.GetBlocksOfType(projectors);
		basePistonsGroup.GetBlocksOfType(basePistons);
		servicePistonsGroup.GetBlocksOfType(servicePistons);
		mainPistonsGroup.GetBlocksOfType(mainPistons);
		rotorsGroup.GetBlocksOfType(rotors);
		lcdsGroup.GetBlocksOfType(lcds);
		solarComputer = GridTerminalSystem.GetBlockWithName(solarComputerName) as IMyProgrammableBlock;
		if (!welders.Any() || !grinders.Any() || !projectors.Any() || !basePistons.Any() || !servicePistons.Any() || !mainPistons.Any() || !rotors.Any() || !lcds.Any() || solarComputer == null) {
			Report("Some necessary blocks were not found");
			return;
		}
	}

	// If the update source is from a trigger or a terminal,
	// this is an interactive command.
	if ((updateType & (UpdateType.Trigger | UpdateType.Terminal)) != 0)
	{
		if (_commandLine.TryParse(argument))
		{
			// Retrieve the first argument. Switches are ignored.
			string command = _commandLine.Argument(0);

			// Now we must validate that the first argument is actually specified, 
			// then attempt to find the matching command delegate.
			if (command == null) 
			{
				Report("No command specified");
			} 
			else
			{
				if (_stateMachine != null)
				{
					Report("Stopping execution of the previous command");
					
					_stateMachine.Dispose();
					_stateMachine = null;
					
					//Abort();
				}
				
				if (string.Equals(command, "deploy", StringComparison.OrdinalIgnoreCase))
				{
					Report("Starting command Deploy");
					_stateMachine = Deploy();
				}
				else if (string.Equals(command, "retract", StringComparison.OrdinalIgnoreCase))
				{
					Report("Starting command Retract");
					_stateMachine = Retract();
				}
				else if (string.Equals(command, "abort", StringComparison.OrdinalIgnoreCase))
				{
					Abort();
				}
				else if (string.Equals(command, "test", StringComparison.OrdinalIgnoreCase))
				{
					_stateMachine = Test();
				}
				else
				{
					Report($"Unknown command {command}");
				}
				
				if (_stateMachine != null)
				{
					Runtime.UpdateFrequency |= UpdateFrequency.Update10;
				}
			}
		}
	}

	// If the update source has this update flag, it means
	// that it's run from the frequency system, and we should
	// run our state machine.
	if ((updateType & UpdateType.Update10) != 0) {
		RunStateMachine();
	}
}

public void RunStateMachine()
{
    // If there is an active state machine, run its next instruction set.
    if (_stateMachine != null) 
    {
        // The MoveNext method is the most important part of this system. When you call
        // MoveNext, your method is invoked until it hits a `yield return` statement.
        // Once that happens, your method is halted and flow control returns _here_.
        // At this point, MoveNext will return `true` since there's more code in your
        // method to execute. Once your method reaches its end and there are no more
        // yields, MoveNext will return false to signal that the method has completed.
        // The actual return value of your yields are unimportant to the actual state
        // machine.
        bool hasMoreSteps = _stateMachine.MoveNext();

        // If there are no more instructions, of if the corutine indicates it wants to stop (be yield return false),
		// we stop and release the state machine.
        if (!hasMoreSteps || !_stateMachine.Current)
        {
            _stateMachine.Dispose();
            _stateMachine = null;
			Runtime.UpdateFrequency = 0;
			
			//Report("Program execution finished");
        }
    }
}

public void Report(string msg) {
	Me.CustomData = msg;
	Echo(msg);
}

public class Timer {
	DateTime _endTime;
	Program _p;
	string _taskMsg;
	int _echoDots = 0;
	int _echoDotsWait = 0;
	
	public bool YieldStatus { get; set; } = true;
	
    public Timer(double seconds, Program program = null, string taskMsg = null) {
		_endTime = DateTime.Now.AddSeconds(seconds);
		_p = program;
		_taskMsg = taskMsg;
		if (_taskMsg == null) {
			_taskMsg = "Waiting";
		}
	}
	
    public bool Active {
		get { return DateTime.Now < _endTime; }
	}
	
	public double Remaining {
		get { return (_endTime - DateTime.Now).TotalSeconds; }
	}
	
	public bool ActiveStatus() {
		if (Active) {
			_p.Report($"{_taskMsg}" + new String('.', _echoDots));
			if (_echoDotsWait++ > 4) {
				_echoDots = (_echoDots + 1) % 4;
				_echoDotsWait = 0;
			}
		}
		else {
			_p.Report($"Error: {_taskMsg}");
		}
		return Active;
	}
	
	public bool ActiveStatusDelayed() {
		if (YieldStatus) {
			if(ActiveStatus()) {
				return true;
			}
			else {
				YieldStatus = false;
				return true;
			}
		}
		else {
			return false;
		}
	}
}
#region PreludeFooter
    }
}
#endregion